from django.conf.urls import url
from .views import *
from django.contrib.auth import views as auth_views

app_name = 'igbt'

urlpatterns = [
    url(r'login/$', auth_views.login, name='login'),
    url(r'logout/$', auth_views.logout,
        {'template_name': 'registration/logout.html'}, name='logout'),
    url(r'password_change/$', auth_views.password_change,
        {'post_change_redirect': 'igbt:password_change_done',
         'template_name': 'registration/password_change.html'},
        name='password_change'),
    url(r'password_change/done/$', auth_views.password_change_done,
        {'template_name': 'registration/password_change_successfully.html'},
        name='password_change_done'),
    url(r'json_znp/$', json_znp),
    url(r'home/$', Start_page.as_view(), name='home'),
    url(r'igbt/$', Page_of_choice.as_view(), name='igbt_choice'),
    url(r'igbt/static_cold_measurements/$', Igbt_static_cold_modules.as_view(),
        name='igbt_static_cold_modules'),
    url(r'igbt/static_hot_measurements/$', Igbt_static_hot_modules.as_view(),
        name='igbt_static_hot_modules'),
    url(r'igbt/dinamic_cold_measurements/$',
        Igbt_dinamic_cold_modules.as_view(), name='igbt_dinamic_cold_modules'),
    url(r'igbt/dinamic_hot_measurements/$', Igbt_dinamic_hot_modules.as_view(),
        name='igbt_dinamic_hot_modules'),
    url(r'igbt/static_cold_measurements/(?P<pk>\d+_\d{4}(_\d){0,1})/$',
        Igbt_static_cold_module.as_view(), name='igbt_static_cold_module'),
    url(r'igbt/static_hot_measurements/(?P<pk>\d+_\d{4}(_\d){0,1})/$',
        Igbt_static_hot_module.as_view(), name='igbt_static_hot_module'),
    url(r'igbt/dinamic_cold_measurements/(?P<pk>\d+_\d{4}(_\d){0,1})/$',
        Igbt_dinamic_cold_module.as_view(), name='igbt_dinamic_cold_module'),
    url(r'igbt/dinamic_hot_measurements/(?P<pk>\d+_\d{4}(_\d){0,1})/$',
        Igbt_dinamic_hot_module.as_view(), name='igbt_dinamic_hot_module'),
    url(r'xlsx_export/$', Xlsx_export.as_view(), name='xlsx'),
]
