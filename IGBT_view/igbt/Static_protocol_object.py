import os.path
import time
import datetime
import re
import sys


class Static_protocol():

    '''Создаёт объект протокола измерения статических параметров'''

    def __init__(self, file):

        with open(file, 'r', encoding='windows-1251') as fl:
            try:
                self.file = list(map(lambda x: x.rstrip('\n'), fl.readlines()))
            except :
                sys.excepthook(*sys.exc_info())

        self.module_type = self.file.pop(0).split('\t')[1]
        self.test_config = self.file.pop(0).split('\t')[1].split(' ')[0]
        x = os.path.split(file)[1][:-4].split('_')

        for y in x:
            if re.search('^\d+C$', y):
                self.temperature = y[:-1]+'°'+y[-1]
                try:
                    x.remove(y)
                except:
                    pass

        self.znp = '_'.join(x)
        self.time = datetime.datetime(*time.localtime(os.path.getmtime(file))[:6])
        self.create_measure_parametrs()
        self.create_result_measurements()
        self.add_limits()

        del self.file

    def create_measure_parametrs(self):

        '''Определяет список параметров'''

        if not self.file[0]:
            del self.file[0]

        self.measure_parametrs_headers = self.file.pop(0).split('\t')
        self.measure_parametrs_values = []
        while True:
            if not self.file[0]:
                del self.file[0]
                break

            self.measure_parametrs_values.append(self.file.pop(0).split('\t'))

    def create_result_measurements(self):

        '''Создаёт список результатов измерений'''

        par_keys_part1 = self.file.pop(0).split('\t')

        par_keys_part2 = self.file.pop(0).split('\t')
        final_index = par_keys_part2.index('')  # определяет начальный индекс
                                                # данных нетребующихся для
                                                # включения в отчёт

        primary_index = 5
        par_keys_part1 = [x.replace(' ', '_') for x in par_keys_part1[primary_index:final_index]]
        par_keys_part2 = par_keys_part2[primary_index:final_index]

        self.result_measurements_headers = [par_keys_part2, par_keys_part1]

        self.result_measurements_values = []
        for x in self.file:

            preliminary_list = x.split('\t')[primary_index:final_index]
            final_list = preliminary_list[0:4]
            for y in preliminary_list[4:]:
                try:
                    final_list.append(int(y.replace(',', '.')))
                except:
                    try:
                        final_list.append(float(y.replace(',', '.')))
                    except:
                        final_list.append(y)

            self.result_measurements_values.append(final_list)

    def add_limits(self):

        limits = ['', ] * 4
        for x in self.measure_parametrs_values:

            y = x[4].split(' ')

            try:
                z1 = int(y[0].replace(',', '.'))
            except:
                z1 = float(y[0].replace(',', '.'))

            try:
                z2 = int(y[2].replace(',', '.'))
            except:                    
                z2 = float(y[2].replace(',', '.'))

            limits.append([z1, z2])

        f = []
        for x in self.result_measurements_values:
            d = []
            for y, l in zip(x, limits):
                if y == 'FAIL':
                    ok_no = 'no'
                elif l and y:
                    if l[0] <= y <= l[1]:
                        ok_no = 'ok'
                    else:
                        ok_no = 'no'
                else:
                    ok_no = 'ok'
                d.append((y, ok_no))
            f.append(d)
        self.result_measurements_values_and_limits = f


if __name__ == '__main__':

    x = Static_protocol('16621_0003_25C.txt')

    print(x.znp, x.time)
    print(x.module_type,)
    print(x.test_config)
    print(x.temperature)

    print(x.measure_parametrs_headers)

    for c in x.measure_parametrs_values:
        print(c)

    for c in x.result_measurements_headers:
        print(c)

    for c in x.result_measurements_values:
        print(c)

    for c in x.result_measurements_values_and_limits:
        print(c)

    input()
