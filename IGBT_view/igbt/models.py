from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class CommonIGBT(models.Model):

    znp = models.CharField(max_length=25, primary_key=True)
    time = models.DateTimeField()
    module_type = models.CharField(max_length=20)
    temperature = models.CharField(max_length=5)
    test_config = models.CharField(max_length=50)
    protocol_object = models.TextField()

    class Meta:
        abstract = True


class IGBT_static_cold (CommonIGBT):

    class Meta():
        db_table = 'igbt_static_cold'
        ordering = ('-time',)

    def __str__(self):
        return self.znp


class IGBT_static_hot (CommonIGBT):

    class Meta():
        db_table = 'igbt_static_hot'
        ordering = ('-time',)

    def __str__(self):
        return self.znp


class IGBT_dinamic_cold (CommonIGBT):

    class Meta():
        db_table = 'igbt_dinamic_cold'
        ordering = ('-time',)

    def __str__(self):
        return self.znp


class IGBT_dinamic_hot (CommonIGBT):

    class Meta():
        db_table = 'igbt_dinamic_hot'
        ordering = ('-time',)

    def __str__(self):
        return self.znp


class IGBT_module_types (models.Model):

    class Meta():
        db_table = 'igbt_module_types'

    module_type = models.CharField(max_length=20, primary_key=True)

    def __str__(self):
        return self.module_type


class Profile(models.Model):

    class Meta():
        db_table = 'profile'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    patronymic = models.CharField(max_length=20, blank=True)
    post = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
