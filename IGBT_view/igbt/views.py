from django.views.generic import ListView, DetailView, RedirectView
from .models import IGBT_static_cold, IGBT_static_hot, IGBT_dinamic_cold, IGBT_dinamic_hot
import pickle
import base64
from django.views.generic.base import TemplateView, View
from pure_pagination.mixins import PaginationMixin
from .forms import Filtr, Filtr2, Sort, sorting_list
import datetime
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from io import BytesIO
import xlsxwriter
from django.views.generic.list import MultipleObjectMixin


class Home(RedirectView):

    url = '/home/'


class Start_page(TemplateView):

    template_name = 'igbt/home.html'

    def get_context_data(self, **kwargs):
        context = super(Start_page, self).get_context_data(**kwargs)
        context['title'] = 'IGBT'
        return context


class Page_of_choice(TemplateView):

    template_name = 'igbt/choice.html'

    def get_context_data(self, **kwargs):
        context = super(Page_of_choice, self).get_context_data(**kwargs)
        context['title'] = 'IGBT'
        return context


class Igbt_modules_Mixin(MultipleObjectMixin):

    sorting_list_part = [y[0] for y in sorting_list]
    context_object_name = 'znp_list'

    def get_context_data(self, **kwargs):

        context = super(Igbt_modules_Mixin, self).get_context_data(**kwargs)

        context['form1'] = self.form1
        context['form2'] = self.form2
        context['sort'] = self.sort
        context['in'] = self.panel_in
        return context

    def get_queryset(self):

        ob_set = self.model.objects.all()

        if self.request.method == 'GET' and len(self.request.GET) > 1:

            self.panel_in = 'in'
            form1 = Filtr(self.request.GET)
            form2 = Filtr2(self.request.GET)
            sort = Sort(self.request.GET)

            if form1.is_valid():
                self.form1 = form1
                if self.form1.cleaned_data['date_1']:
                    ob_set = ob_set.filter(
                        time__gte=self.form1.cleaned_data['date_1'])
                if self.form1.cleaned_data['date_2']:
                    ob_set = ob_set.filter(
                        time__lte=self.form1.cleaned_data['date_2'])
            else:
                self.form1 = Filtr()

            if form2.is_valid():
                self.form2 = form2
                if self.form2.cleaned_data['znp']:
                    ob_set = ob_set.filter(
                        znp__startswith=self.form2.cleaned_data['znp'])
                if self.form2.cleaned_data['module_type']:
                    ob_set = ob_set.filter(
                        module_type__exact=self.request.GET['module_type'])
            else:
                self.form2 = Filtr2()

            if sort.is_valid():
                self.sort = sort
                if self.sort.cleaned_data['sorting']:
                    ob_set = ob_set.order_by(self.request.GET['sorting'])
            else:
                self.sort = Sort()

            return ob_set

        else:
            self.panel_in = ''
            self.form1 = Filtr()
            self.form2 = Filtr2()
            self.sort = Sort()
            return ob_set


class Base_Igbt_modules(LoginRequiredMixin, PaginationMixin,
                        ListView, Igbt_modules_Mixin):

    login_url = '/login/'
    paginate_by = 15
    template_name = 'igbt/module_list.html'


class Igbt_static_cold_modules(Base_Igbt_modules):

    model = IGBT_static_cold

    def get_context_data(self, **kwargs):

        context = super(Igbt_static_cold_modules,
                        self).get_context_data(**kwargs)

        context['tps_cold'] = 'active'
        context['title'] = 'TPS 625 (cold)'
        return context


class Igbt_static_hot_modules(Base_Igbt_modules):

    model = IGBT_static_hot

    def get_context_data(self, **kwargs):

        context = super(Igbt_static_hot_modules,
                        self).get_context_data(**kwargs)

        context['tps_hot'] = 'active'
        context['title'] = 'TPS 625 (hot)'
        return context


class Igbt_dinamic_cold_modules(Base_Igbt_modules):

    model = IGBT_dinamic_cold

    def get_context_data(self, **kwargs):

        context = super(Igbt_dinamic_cold_modules,
                        self).get_context_data(**kwargs)

        context['dts_cold'] = 'active'
        context['title'] = 'DTS 758 (cold)'
        return context


class Igbt_dinamic_hot_modules(Base_Igbt_modules):

    model = IGBT_dinamic_hot

    def get_context_data(self, **kwargs):

        context = super(Igbt_dinamic_hot_modules,
                        self).get_context_data(**kwargs)

        context['dts_hot'] = 'active'
        context['title'] = 'DTS 758 (hot)'
        return context


class Base_Igbt_module(LoginRequiredMixin, DetailView):

    login_url = '/login/'
    template_name = 'igbt/module_results.html'

    def get_context_data(self, **kwargs):

        context = super(Base_Igbt_module, self).get_context_data(**kwargs)
        prot_obj = pickle.loads(base64.b64decode(
            context['object'].protocol_object))
        context['prot_obj'] = prot_obj
        context['title'] = context['object'].znp
        return context


class Igbt_static_cold_module(Base_Igbt_module):

    model = IGBT_static_cold


class Igbt_static_hot_module(Base_Igbt_module):

    model = IGBT_static_hot


class Igbt_dinamic_cold_module(Base_Igbt_module):

    model = IGBT_dinamic_cold


class Igbt_dinamic_hot_module(Base_Igbt_module):

    model = IGBT_dinamic_hot


class Xlsx_export(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):

        k = request.GET['path'].split('/')
        x = k[2].split('_')
        znp = k[3]

        model_name = '_'.join(('IGBT', x[0], x[1]))
        pr_tb = globals()[model_name].objects.get(znp=znp)
        filename = '_'.join((pr_tb.znp, pr_tb.module_type,
                             pr_tb.temperature, x[0].upper()))
        pr_ob = pickle.loads(base64.b64decode(pr_tb.protocol_object))
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename='+filename+'.xlsx'
        xlsx_data = self.write_To_Excel(pr_ob, request)
        response.write(xlsx_data)
        return response

    def write_To_Excel(self, pr_ob, request):

        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet_1 = workbook.add_worksheet("Results")
        worksheet_1.set_landscape()
        worksheet_1.set_margins(left=0.3, right=0.3, top=0.3, bottom=0.3)

        format_1 = workbook.add_format({'border': 1})
        format_2 = workbook.add_format({'border': 1, 'bold': True})
        format_2.set_text_wrap()
        format_2.set_align('top')
        format_3 = workbook.add_format({'left': 1})
        format_4 = workbook.add_format({'right': 1})
        format_5 = workbook.add_format({'left': 1, 'top': 1, 'bottom': 1})
        format_6 = workbook.add_format({'top': 1, 'right': 1, 'bottom': 1})
        format_7 = workbook.add_format({'right': 1, 'bottom': 1})
        format_8 = workbook.add_format({'top': 1, 'bottom': 1})
        format_9 = workbook.add_format({'bottom': 1})
        format_10 = workbook.add_format({'left': 1, 'bottom': 1})
        format_11 = workbook.add_format({'border': 1, 'bg_color': '#DCDCDC'})
        format_12 = workbook.add_format({'border': 1, 'bg_color': '#DCDCDC',
                                         'bold': True})

        worksheet_1.write(0, 0, '', format_5)
        worksheet_1.write(0, 1, 'Общие данные', format_8)
        worksheet_1.write(0, 2, '', format_6)
        worksheet_1.write(16, 0, '', format_10)
        worksheet_1.write(16, 2, '', format_7)
        worksheet_1.write(16, 1, '', format_9)

        for i in range(1, 16):
            worksheet_1.write(i, 0, '', format_3)
            worksheet_1.write(i, 2, '', format_4)

        worksheet_1.set_column('A:A', 1)
        worksheet_1.set_column('B:B', 32)
        worksheet_1.set_column('C:D', 1)

        worksheet_1.write('B3', 'Номер ЗНП', format_1)
        worksheet_1.write('B4', pr_ob.znp, format_2)
        worksheet_1.write('B6', 'Тип модуля', format_1)
        worksheet_1.write('B7', pr_ob.module_type, format_2)
        worksheet_1.write('B9', 'Температура', format_1)
        worksheet_1.write('B10', pr_ob.temperature, format_2)
        worksheet_1.write('B12', 'Дата измерения', format_1)

        date_format_str = 'dd/mm/yy hh:mm'
        date_format = workbook.add_format({'bold': True,
                                           'num_format': date_format_str,
                                           'align': 'left',
                                           'border': 1}
                                          )

        d = datetime.timedelta(seconds=+10800)
        naive_datetime = pr_ob.time+d
        naive_datetime = naive_datetime.replace(tzinfo=None)

        worksheet_1.write_datetime(12, 1, naive_datetime, date_format)

        worksheet_1.write('B15', 'Тип теста измерений', format_1)
        worksheet_1.write('B16', pr_ob.test_config, format_2)

        if request.GET['add']:

            worksheet_1.write(18, 0, '', format_5)
            worksheet_1.write(18, 1, 'Данные пользователя', format_8)
            worksheet_1.write(18, 2, '', format_6)
            worksheet_1.write(26, 0, '', format_10)
            worksheet_1.write(26, 2, '', format_7)
            worksheet_1.write(26, 1, '', format_9)

            for i in range(19, 26):
                worksheet_1.write(i, 0, '', format_3)
                worksheet_1.write(i, 2, '', format_4)
            try:
                name = '{0} {1}. {2}.'.format(
                    request.user.last_name,
                    request.user.first_name[0],
                    request.user.profile.patronymic[0])
            except:
                name = '{0}'.format(request.user.last_name)

            worksheet_1.write('B21', 'ФИО ', format_1)
            worksheet_1.write('B22', name, format_2)
            worksheet_1.write('B24', 'Должность', format_1)
            try:
                worksheet_1.merge_range('B25:B26', request.user.profile.post,
                                        format_2)
            except:
                worksheet_1.merge_range('B25:B26', '', format_2)

            if request.GET['note']:
                worksheet_1.write(28, 0, '', format_5)
                worksheet_1.write(28, 1, 'Примечание', format_8)
                worksheet_1.write(28, 2, '', format_6)
                worksheet_1.merge_range('A30:C31', request.GET['note'],
                                        format_2)

        i = 4
        for x in pr_ob.result_measurements_headers:
            if i == 4:
                worksheet_1.set_column(i, i, 21)
            else:
                worksheet_1.set_column(i, i, 3)
            t = [p.replace('TEST_', '') for p in x]
            worksheet_1.write_column(0, i, t, format_2)
            i += 1

        for x in pr_ob.result_measurements_values_and_limits:
            worksheet_1.set_column(i, i, 10)
            for r, (y, z) in enumerate(x):
                if z == 'no':
                    if y == 'FAIL':
                        worksheet_1.write(r, i, y, format_12)
                    else:
                        worksheet_1.write(r, i, y, format_11)
                else:
                    if y == 'PASS':
                        worksheet_1.write(r, i, y, format_2)
                    else:
                        worksheet_1.write(r, i, y, format_1)
            i += 1

        worksheet_2 = workbook.add_worksheet("Parametrs")

        worksheet_2.write_row(0, 0, pr_ob.measure_parametrs_headers, format_2)

        i = 1

        for x in pr_ob.measure_parametrs_values:
            width_row = 14
            for y in x:
                phrases = y.replace('\r', '').split('\n')
                width = len(phrases)*14
                if width_row < width:
                    width_row = width
            worksheet_2.write_row(i, 0, x, format_1)
            worksheet_2.set_row(i, width_row)
            i += 1

        col_width_list = [5]*len(pr_ob.measure_parametrs_values[0])

        xxx = pr_ob.measure_parametrs_values[:]
        xxx.append(pr_ob.measure_parametrs_headers)

        for x in xxx:
            width = 5
            for i, y in enumerate(x):
                phrases = y.replace('\r', '').split('\n')
                for phrase in phrases:
                    if width < len(phrase):
                        width = len(phrase)+3
                if col_width_list[i] < width:
                    col_width_list[i] = width
        for i, x in enumerate(col_width_list):
            worksheet_2.set_column(i, i, x)

        workbook.close()
        xlsx_data = output.getvalue()
        # xlsx_data contains the Excel file
        return xlsx_data


@login_required(login_url='/login/')
def json_znp(request):

    x = request.GET['path'].split('/')[2].split('_')
    model_name = '_'.join(('IGBT', x[0], x[1]))

    if request.method == 'GET' and 'term' in request.GET:
        znp_list = list(globals()[model_name].objects.filter(
            znp__startswith=request.GET['term']
            ).order_by('znp').values_list('znp', flat=True)[:7])

        return JsonResponse(znp_list, safe=False)
