from django import forms
from .models import IGBT_module_types


sorting_list = [['-time', 'убыванию дыты'],
                ['time', 'возрастанию даты'],
                ['-znp', 'убыванию ЗНП'],
                ['znp', 'возрастанию ЗНП'],
                ]


def find_module_type():

    no_repeat_module_type_list = list(IGBT_module_types.objects.values_list(
        'module_type', flat=True))
    no_repeat_module_type_list.sort()
    no_repeat_module_type_list.insert(0, '')
    return [[x, x] for x in no_repeat_module_type_list]


class Filtr(forms.Form):

    date_1 = forms.DateTimeField(required=False, label='Период с',
                                 input_formats=['%d.%m.%Y', ])
    date_2 = forms.DateTimeField(required=False, label='Период до',
                                 input_formats=['%d.%m.%Y', ])


class Filtr2(forms.Form):

    znp = forms.CharField(required=False, label='Номер ЗНП', max_length=25)
    module_type = forms.ChoiceField(required=False, label='Тип модуля',
                                    choices=find_module_type)


class Sort(forms.Form):

    sorting = forms.ChoiceField(label='Сортировать по', choices=sorting_list)
