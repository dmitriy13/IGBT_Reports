import psycopg2
import sys
import os


class Db_handler:
    '''Сохраняет данные в postgreSQL'''

    def __init__(self, database, user, password, host='localhost', port=5432):

        self.arg_db = {}
        self.arg_db['database'] = database
        self.arg_db['user'] = user
        self.arg_db['password'] = password
        self.arg_db['host'] = host
        self.arg_db['port'] = port

    def open_connect_to_db(self):

        self.conn = psycopg2.connect(**self.arg_db)
        self.cur = self.conn.cursor()

    def close_connect_to_db(self):

        self.cur.close()
        self.conn.close()

    def add_in_db(self, table_name, znp, time, module_type,
                  temperature, test_config, pr_ob_pick):
        '''Помещает данные в базу данных'''

        self.cur.execute('''INSERT INTO %s (znp, time, module_type,temperature,test_config, protocol_object)\n''' % table_name +
                         '''VALUES (%s, %s, %s, %s, %s, %s)''', (znp, time,
                                                                 module_type,
                                                                 temperature,
                                                                 test_config,
                                                                 pr_ob_pick))

        self.conn.commit()

    def update_db(self, table_name, time, module_type, temperature,
                  test_config, pr_ob_pick, znp):
        '''Обновляет данные в базе данных'''

        self.cur.execute('''UPDATE %s\n''' % table_name +
                        '''SET time = %s, module_type = %s , temperature =  %s, test_config = %s, protocol_object = %s
                        WHERE znp = %s''', (time, module_type, temperature,
                                            test_config, pr_ob_pick, znp))

        self.conn.commit()

    def exist_znp(self, table_name, znp):

        self.cur.execute('''SELECT znp
                                FROM %s\n''' % table_name +
                         '''WHERE znp = %s''', (znp,))
        if self.cur.fetchone():
            return True

    def exist_module_type(self, module_type):

        self.cur.execute('''SELECT module_type
                                FROM igbt_module_types
                                WHERE module_type = %s''', (module_type,))
        if self.cur.fetchone():
            return True

    def add_module_type(self, module_type):
        '''Помещает данные в базу данных'''

        self.cur.execute('''INSERT INTO igbt_module_types (module_type)
                                VALUES (%s)''', (module_type,))

        self.conn.commit()


if __name__ == '__main__':

    database = 'db1'
    user = 'postgres'
    password = 12345

    Db_handler(database, user, password)
