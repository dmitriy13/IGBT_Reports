import xlrd
import datetime


class Dinamic_protocol():

    '''Создаёт объект протокола измерения динамических параметров'''

    def __init__(self, file):

        self.book = xlrd.open_workbook(file)

        sh = self.book.sheet_by_index(0)

        self.module_type = sh.cell_value(rowx=1, colx=1
                                         ).split(' ')[0].split('_')[0]
        self.test_config = sh.cell_value(rowx=1, colx=1).split(' ')[0]
        x = sh.cell_value(rowx=0, colx=1).split('_')
        try:
            x.remove('cold')
        except:
            pass

        try:
            x.remove('hot')
        except:
            pass

        self.znp = '_'.join(x)
        x = sh.cell_value(rowx=1, colx=1).split(' ')[0].split('_')[1]
        self.temperature = x[:-1]+'°'+x[-1]

        sh = self.book.sheet_by_index(1)

        self.time = xlrd.xldate.xldate_as_datetime(sh.row(1)[sh.ncols-1].value,
                                                   0)

        self.create_measure_parametrs()
        self.create_result_measurements()

        del self.book

    def create_measure_parametrs(self):

        '''Определяет список параметров'''

        sh = self.book.sheet_by_index(2)

        self.measure_parametrs_headers = sh.row_values(1)
        del self.measure_parametrs_headers[1]
        del self.measure_parametrs_headers[-1]

        self.measure_parametrs_values = []
        for x in range(2, sh.nrows):
            y = sh.row_values(x)
            del y[1]
            del y[-1]
            self.measure_parametrs_values.append(y)

    def create_result_measurements(self):

        '''Создаёт список результатов измерений'''

        sh = self.book.sheet_by_index(1)

        headers = sh.col_values(0)
        del headers[6]
        headers[0] = 'SERIAL#'
        headers[1] = 'DATE'
        headers_1 = headers[:6]
        headers_2 = ['']*6

        del headers[:6]

        for header in headers:
            if header.startswith('01:'):
                x = '01'
                index_1 = headers.index(header)

            elif header.startswith('02:'):
                x = '02'
                index_2 = headers.index(header)

            else:
                y = header.split(' ')
                headers_1.append(y[4]+' '+y[-1])
                headers_2.append(x)

        headers_1.insert(2, 'TIME')
        headers_2.insert(2, '')

        for i in range(3, 7):
            headers_1[i] = headers_1[i].replace('_', ' ')

        headers_1[6] = headers_1[6].replace('[', ' [')

        self.result_measurements_headers = [headers_1, headers_2]

        try:
            if index_1 > index_2:
                index_1, index_2 = index_2, index_1
        except:
            pass

        self.result_measurements_values = []

        for col in range(5, sh.ncols):

            x = sh.col_values(col)

            try:
                x[0] = str(x[0]).split('.')[0]
            except:
                pass

            del x[6]

            try:
                del x[index_2+6]
            except:
                pass

            try:
                del x[index_1+6]
            except:
                pass

            t = xlrd.xldate.xldate_as_datetime(x[1], 0).isoformat(sep=' '
                                                                  ).split(' ')
            r = t[0].split('-')
            r.reverse()
            x[1] = '.'.join(r)
            x.insert(2, t[1])
            self.result_measurements_values.append(x)

        limits = ['']*7

        try:
            del headers[index_2]
        except:
            pass

        try:
            del headers[index_1]
        except:
            pass

        for x in headers:

            y = x.split(' ')

            try:
                z1 = int(y[5].replace(',', '.'))
            except:
                z1 = float(y[5].replace(',', '.'))

            try:
                z2 = int(y[7].replace(',', '.'))
            except:
                z2 = float(y[7].replace(',', '.'))

            limits.append([z1, z2])

        f = []
        p = []
        for x in self.result_measurements_values:
            d = []
            for y, l in zip(x, limits):
                if y == '':
                    y = '-:-'
                    ok_no = 'no'
                    d[0][1] = 'no'
                elif l and y:
                    if l[0] <= y <= l[1]:
                        ok_no = 'ok'
                    else:
                        ok_no = 'no'
                        d[0][1] = 'no'
                else:
                    ok_no = 'ok'
                d.append([y, ok_no])

            if d[0][0] in p:
                continue
            p.append(d[0][0])
            f.append(d)
        self.result_measurements_values_and_limits = f


if __name__ == '__main__':

    x = Dinamic_protocol('17517_0001_cold.xlsx')

    print(x.znp, x.time)
    print(x.module_type,)
    print(x.test_config)
    print(x.temperature)

    print(x.measure_parametrs_headers)

    for c in x.measure_parametrs_values:
        print(c)

    for c in x.result_measurements_headers:
        print(c)

    for c in x.result_measurements_values:
        print(c)

    for c in x.result_measurements_values_and_limits:
        print(c)

    input()
