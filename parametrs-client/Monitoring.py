import sys
import time
from threading import Lock
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class My_EventHandler(FileSystemEventHandler):

    '''Объект мониторинга директории с переопределёнными методами родителя'''

    def __init__(self, lock, set_box, change_of_file):
        FileSystemEventHandler.__init__(self)
        self.lock = lock
        self.box = set_box
        self.change_of_file = change_of_file

    def on_created(self, event):
        pass

    def on_deleted(self, event):
        with self.lock:
            if not event.is_directory:
                # print(event.is_directory)
                # print(1,event.src_path)
                self.box.discard(event.src_path)
                self.change_of_file[0] = 1
                # print(self.box)

    def on_modified(self, event):
        with self.lock:
            if not event.is_directory:
                # print(event.is_directory)
                # print(2,event.src_path)
                self.box.add((event.src_path))
                self.change_of_file[0] = 1
                # print(self.box)

    def on_moved(self, event):
        with self.lock:
            if not event.is_directory:
                # print(3,event.src_path, event.dest_path)
                self.box.discard(event.src_path)
                self.change_of_file[0] = 1
                # print(self.box)


if __name__ == "__main__":

    lock = Lock()
    d = set()
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = My_EventHandler(lock, d)
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(d)
        observer.stop()
    observer.join()
