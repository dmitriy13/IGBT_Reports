import os.path
import re
import sys
import psutil
import datetime


def log_in_file(addition=''):

    '''Записывает пойманые исключения в файл logfile.log'''

    x = sys.stderr
    sys.stderr = open('logfile.log', 'a')
    sys.stderr.write('\n\n'+datetime.datetime.now().isoformat(sep=' ')
                     + '  '+addition+'\n')
    sys.excepthook(*sys.exc_info())
    sys.stderr.close()
    sys.stderr = x


def match_regex_isfile(path):

    '''Сопоставление рег. выр.'''

    search_str = '^\d+_\d{4}(_\d){0,1}_((\d{2,3}C)|cold|hot)(_\d){0,1}.(txt|xlsx)$'
    if os.path.isfile(path) and re.search(search_str, os.path.split(path)[1]):
        return True
    else:
        return False


def match_regex_is_static_or_dinamic(path):

    '''Определение таблицы и объекта протокола'''

    name = os.path.split(path)[1]
    if re.search('^\d+_\d{4}(_\d){0,1}_25C(_\d){0,1}.txt$', name):
        return ('igbt_static_cold', 1)
    elif re.search('^\d+_\d{4}(_\d){0,1}_1\d{2}C(_\d){0,1}.txt$', name):
        return ('igbt_static_hot', 1)
    elif re.search('^\d+_\d{4}(_\d){0,1}_cold(_\d){0,1}.xlsx$', name):
        return ('igbt_dinamic_cold', 0)
    elif re.search('^\d+_\d{4}(_\d){0,1}_hot(_\d){0,1}.xlsx$', name):
        return ('igbt_dinamic_hot', 0)


def if_programm_run(process_name):

    '''Возвращает True, если процесс, название которого передано в
    аргументе, не запущен'''

    try:
        name_and_id_process_list = [(psutil.Process(id).name(), id) for id in psutil.pids()]
        # print(name_and_id_process_list)
        for x in name_and_id_process_list:
            if process_name in x:
                return False
        return True
    except psutil.NoSuchProcess:
        pass
    except:
        # sys.excepthook(*sys.exc_info())
        log_in_file()
        return False


if __name__ == '__main__':
    print(match_regex_isfile('.\\Functions.py'))
    print(match_regex_isfile('.\\16616_0002_25C.txt'))
    print(if_programm_run('chrome.exe'))

    input()
