from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from tkinter.filedialog import askdirectory
from threading import Lock
import queue
import sys
import os
import shelve
import threading
import json
import pickle
import base64
import psycopg2
from Monitoring import Observer, My_EventHandler
from File_filter import *
from Static_protocol_object import Static_protocol
from Dinamic_protocol_object import Dinamic_protocol
from Db_handler import Db_handler
from Tray import SysTrayIcon

dataQueue = queue.Queue()


class AutoScrollbar(ttk.Scrollbar):

    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.

    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)

    def pack(self, **kw):
        raise TclError

    def place(self, **kw):
        raise TclError


class Preferences(Toplevel):

    '''Окно с настройками'''

    def __init__(self):

        showinfo('Preferences',
                 'If you change the directory, the queue will be remove')

        Toplevel.__init__(self, padx=10, pady=10)
        self.title('Preferences')

        self.geometry('370x140'+'+'+str(int((root.winfo_width()-370)
                                            + root.winfo_x()))
                      + '+'+str(int((root.winfo_height()-120)
                                    + root.winfo_y())))

        self.overrideredirect(True)
        self.resizable(False, False)
        self.makeWidgets()
        self.focus()
        self.iconbitmap(bitmap='image/IGBT.ico')
        self.focus_set()
        self.grab_set()

    def makeWidgets(self):

        change_catalogs = LabelFrame(self, text='Change catalogs')
        change_catalogs.pack(expand=YES, fill=BOTH, padx=2)

        self.entries = []

        catalog = shelve.open('Preferences\\catalog')

        for item, field in enumerate([('Input catalog', 'reports_txt'), ]):

            row = LabelFrame(change_catalogs, text=field[0])
            ent = ttk.Entry(row, width=40)
            row.pack(side=TOP, fill=X, pady=3, padx=3)
            ent.pack(side=LEFT, expand=YES, fill=X, pady=5, padx=5)

            try:
                ent.insert(0, catalog[field[1]])
            except KeyError:
                ent.insert(0, os.getcwd()+'\\'+field[1])

            ttk.Button(row, text='Change',
                       command=lambda item=item: self.change_catalog(item)
                       ).pack(side=LEFT, padx=3)

            self.entries.append(ent)

        for text, call in (('Ok', lambda: self.seveCatalog(ok=True)),
                           ('Apply', self.seveCatalog),
                           ('Cencel', self.destroy)):

            ttk.Button(change_catalogs, text=text,
                       command=call).pack(side=RIGHT, padx=5)
        catalog.close()

    def change_catalog(self, item):

        pathCatalog = askdirectory().replace('/', '\\')
        if pathCatalog != '':
            self.entries[item].delete(0, END)
            self.entries[item].insert(0, pathCatalog)
        self.entries[item].focus()

    def seveCatalog(self, ok=False):

        catalog = shelve.open('Preferences\\catalog')

        for item, nameShelve in enumerate(['reports_txt', ]):
            if os.path.isdir(self.entries[item].get()):
                catalog[nameShelve] = self.entries[item].get()
                if item == 0:
                    mon.after_change_dir(self.entries[item].get())

            else:
                showerror('Error', 'Directoty not exists!')
                self.entries[item].focus()
                return

        catalog.close()

        if ok:
            self.destroy()


class Tray_Thread(threading.Thread):

    '''Отдельный поток для значка в трее'''

    def __init__(self, dataQueue):
        self.dataQueue = dataQueue
        threading.Thread.__init__(self)

    def run(self):

        hover_text = "IGBT reports client"

        def switch_icon(sysTrayIcon, icon):
            sysTrayIcon.icon = icon
            sysTrayIcon.refresh_icon()

        def preferences(sysTrayIcon): self.dataQueue.put('preferences')

        def show_window(sysTrayIcon): self.dataQueue.put('show')

        def run_monitoring(sysTrayIcon):
            self.dataQueue.put('run')
            switch_icon(sysTrayIcon, 'image/igbt.ico')

        def stop_monitoring(sysTrayIcon):

            if askokcancel('Monitoring',
                           'Monitoring will be desable. \nAre you sure?'):
                self.dataQueue.put('stop')
                switch_icon(sysTrayIcon, 'image/igbt_off.ico')

        def bye(sysTrayIcon): self.dataQueue.put('Quit')

        menu_options = (('Show window', None, show_window),
                        ('Monitoring', None, (('Run monitoring',
                                               None, run_monitoring),
                                              ('Stop monitoring',
                                               None, stop_monitoring))),
                        ('Setting', None, (('Preferences',
                                            None, preferences),)))
        try:
            SysTrayIcon('image/igbt.ico', hover_text, menu_options,
                        on_quit=bye, default_menu_index=0)
        except:
            log_in_file()


class MenuBar():

    '''Создаёт меню окна'''

    def __init__(self, parent):

        self.parent = parent
        self.MakeMenuBar()

    def MakeMenuBar(self):

        self.menubar = Menu(self.parent)
        self.parent.config(menu=self.menubar)
        self.settingMenu()

    def settingMenu(self):

        pulldown = Menu(self.menubar, tearoff=False)
        pulldown.add_command(label='Preferences', command=Preferences)
        self.menubar.add_cascade(label='Setting', underline=0, menu=pulldown)


class Monitoring_and_View_Text(LabelFrame):

    '''Объект мониторинга директории, отображения списка протоколов,
      создания объектов протокола и добавления их в базу данных'''

    def __init__(self, text, json_file):

        LabelFrame.__init__(self, root, text=text)
        self.pack(expand=YES, fill=BOTH, padx=7, pady=10)

        self.lock = Lock()
        self.defoult_catalog = os.getcwd()

        self.json_file = json_file
        self.change_of_file = [1, ]

        self.program_name_not_exist = 'tps746.exe'

        with open('Preferences\\db_config.json') as fl:
            try:
                self.db_data = json.load(fl)
            except json.JSONDecodeError:
                log_in_file()
                sys.exit(1)

        self.db_handler = Db_handler(**self.db_data)

        with open(json_file) as fl:
            try:
                self.json_data = set(json.load(fl))
            except json.JSONDecodeError:
                self.json_data = set()

        self.catalogShelve = shelve.open('Preferences\\catalog')
        try:
            self.path = self.catalogShelve['reports_txt']
        except KeyError:
            self.path = self.defoult_catalog

        if not os.path.isdir(self.path):
            self.path = self.defoult_catalog

        self.catalogShelve.close()
        del self.catalogShelve

        self.makeWidgets()
        self.create_and_run_monitiring()
        self.process_set()
        self.process_Queue()
        self.prog_is_running(self.program_name_not_exist)

    def makeWidgets(self):

        self.lbl = ttk.Label(self, text=self.path, width=10)
        self.lbl.grid(row=0, column=0, sticky=EW, pady=2,
                      columnspan=2, padx=15)
        sbar_y = AutoScrollbar(self)
        sbar_x = AutoScrollbar(self, orient='horizontal')
        text = Text(master=self, relief=SUNKEN, wrap='none')
        sbar_y.config(command=text.yview)
        sbar_x.config(command=text.xview)
        text.config(yscrollcommand=sbar_y.set)
        text.config(xscrollcommand=sbar_x.set)
        text.config(padx=5, pady=5, width=42, height=9)
        text.grid(row=1, column=0, padx=5, pady=5, sticky=NSEW)
        sbar_y.grid(row=1, column=1, sticky=NSEW)
        sbar_x.grid(row=2, column=0, sticky=NSEW)
        self.text = text

        self.f = LabelFrame(self)
        self.f.grid(row=3, column=0, sticky=EW, pady=2, columnspan=2, padx=15)
        self.lblm = ttk.Label(self.f, text='Monitoring: off', width=10)
        self.lblm.pack(side=LEFT, pady=2, expand=YES, fill=BOTH, padx=15)

    def create_and_run_monitiring(self):

        '''Мониторинг директории в отдельном потоке выполнения'''

        if self.lblm['text'] == 'Monitoring: off':
            self.event_handler = My_EventHandler(self.lock, self.json_data,
                                                 self.change_of_file)
            self.observer = Observer()
            self.observer.schedule(self.event_handler, self.path,
                                   recursive=False)
            self.observer.start()
            self.lblm.config(text='Monitoring: on')

    def stop_monitoring(self):

        '''Остановка мониторинга'''

        if self.lblm['text'] == 'Monitoring: on':
            self.observer.stop()
            self.lblm.config(text='Monitoring: off')
            del self.observer
            del self.event_handler

    def after_change_dir(self, path):

        '''Очищает очередь после смены директории для мониторинга'''

        self.lbl.config(text=path)
        self.path = path
        self.stop_monitoring()

        with self.lock:
            self.finish_list = []
            self.json_data = set()
            with open(self.json_file, 'w') as fl:
                pass

        self.create_and_run_monitiring()

    def process_set(self):

        '''Выводит список протоколов, которые будут обработаны и добавлены
        в БД'''

        with self.lock:
            if self.change_of_file[0]:
                try:
                    json_data = json.dumps(list(self.json_data), indent=2)
                except json.JSONDecodeError:
                    sys.excepthook(*sys.exc_info())
                with open(self.json_file, 'w') as fl:
                    fl.write(json_data)

                self.finish_list = list(filter(match_regex_isfile,
                                               list(self.json_data)))

                self.change_of_file[0] = 0

                x = '\n'.join(list(map(lambda x: os.path.split(x)[1],
                                       self.finish_list[:])))
                self.settext(x)

        self.after(2000, self.process_set)

    def process_Queue(self):

        '''Обрабатывает команды, переданные из трея'''

        try:
            command = dataQueue.get(block=False)
        except queue.Empty:
            command = 0

        if command == 'Quit':
            root.destroy()
        elif command == 'run':
            self.create_and_run_monitiring()
        elif command == 'stop':
            self.stop_monitoring()
        elif command == 'show':
            root.deiconify()
            root.focus_force()
        elif command == 'preferences':
            Preferences()

        self.after(500, self.process_Queue)

    def prog_is_running(self, program_name):

        '''Проверяет запущена ли программа шустера'''

        if if_programm_run(program_name) and self.finish_list:
            self.create_report_object_and_add_in_db()

        self.after(2000, self.prog_is_running, self.program_name_not_exist)

    def create_report_object_and_add_in_db(self):

        try:
            self.db_handler.open_connect_to_db()
        except psycopg2.Error:
            return

        with self.lock:
            x = self.finish_list[0]

        try:
            table_name, is_stat = match_regex_is_static_or_dinamic(x)
            if is_stat:
                pr_ob = Static_protocol(x)
                pr_ob_pick = base64.b64encode(pickle.dumps(pr_ob)).decode()
            else:
                pr_ob = Dinamic_protocol(x)
                pr_ob_pick = base64.b64encode(pickle.dumps(pr_ob)).decode()
        except: 
            log_in_file(x)
            with self.lock:
                self.json_data.discard(self.finish_list.pop(0))
            showerror('Warning', x +
                      ' is damaged. The protocol object  is not created.')
            self.change_of_file[0] = 1
            return

        try:
            if not self.db_handler.exist_znp(table_name, pr_ob.znp):
                self.db_handler.add_in_db(table_name, pr_ob.znp,
                                          pr_ob.time, pr_ob.module_type,
                                          pr_ob.temperature, pr_ob.test_config,
                                          pr_ob_pick)
            else:
                self.db_handler.update_db(table_name, pr_ob.time,
                                          pr_ob.module_type, pr_ob.temperature,
                                          pr_ob.test_config, pr_ob_pick,
                                          pr_ob.znp)
            if not self.db_handler.exist_module_type(pr_ob.module_type):
                self.db_handler.add_module_type(pr_ob.module_type)

        except psycopg2.Error:
            log_in_file()
            return
        else:
            self.json_data.discard(self.finish_list.pop(0))
            self.change_of_file[0] = 1
            self.db_handler.close_connect_to_db()

    def settext(self, text):

        '''Выводит текст'''

        self.text.config(state=NORMAL)
        self.text.delete('1.0', END)
        self.text.insert('1.0', text)
        self.text.mark_set(INSERT, '1.0')
        self.text.config(state=DISABLED)


if __name__ == "__main__":
    root = Tk()
    x = root.winfo_screenwidth()
    y = root.winfo_screenheight()

    root.geometry('400x300+'+str(int((x-400)))+'+'+str(int((y-330))))
    root.title('IGBT reports client')
    root.resizable(False, False)
    root.iconbitmap(bitmap='image/igbt.ico')
    root.bind('<FocusOut>', lambda x: root.withdraw())
    root.overrideredirect(True)
    root.topmost = True
    menu = MenuBar(root)

    try:
        mon = Monitoring_and_View_Text(text="Modified files",
                                       json_file='Preferences\\list.json')
    except:
        log_in_file()
        raise

    Tray_Thread(dataQueue).start()

    try:
        root.mainloop()
    except:
        log_in_file()
        raise
